#!/Python27/python

# Function definition is here
def hello_world():
    return 'Hello World!'

# Function definition is here
def printme( str ):
   "This prints a passed string into this function"
   print str
   return;

# Function definition is here
def printinfo( name, age ):
   "This prints a passed info into this function"
   print "Name: ", name
   print "Age ", age
   return;

print "Content-Type: text/plain; charset=utf-8"
print

if __name__ == '__main__':
    print hello_world()

# Now you can call printme function
printme("I'm first call to user defined function!")
printme("Again second call to the same function")

# Now you can call printinfo function
printinfo( age=50, name="miki" )
