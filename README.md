# Python: CGI

## What is CGI?

    * The Common Gateway Interface, or CGI, is a standard for external gateway programs to interface with information servers such as HTTP servers.

    * The current version is CGI/1.1 and CGI/1.2 is under progress.

## CGI vs WSGI

    If you want to work on CGI, then you must turn off the mod_wsgi module in your Apache config file,

    #LoadModule wsgi_module modules/mod_wsgi.so

    and,

    ```
    # DocumentRootDirectory
    <Directory "${path}/data/localweb">

    ...

    #AddHandler wsgi-script .py

    ...

    ```

## ref:

    * http://www.tutorialspoint.com/python/python_cgi_programming.htm

# Python: Configuring the Apache Web Server to Run Python on Windows

1. Install Python.

2. Configure Apache to run Python CGI.

    Open the httpd.conf apache configuration file located in the apache install directory in the conf directory. search for the following:

    `#AddHandler cgi-script .cgi`

    Uncomment this line by removing the # in front of the line, and add a .py to the end of the line. The new line should look like this:

    `AddHandler cgi-script .cgi .py`

3. Allow .cgi script Access.

    Look for the alias that you define in the EasyPHP Administration screen, and then add the "ExecCGI" directive as shown in bold below (note that your alias name and directories will be deferent):

    ```
    Alias "/My developement folder" "C:/www/mywebdev"
    <Directory "C:/www/mywebdev">
        Options FollowSymLinks Indexes ExecCGI
        AllowOverride All
        Order deny,allow
        Allow from 127.0.0.1
        deny from all
    </Directory>
    ```

4. Restart Apache.

5. Run a test Python page. The following is an example of the template:

    ```
    #!/usr/bin/python
    print "Content-type: text/html"
    print
    print "<html><head>"
    print ""
    print "</head><body>"
    print "Hello."
    print "</body></html>"
    ```

    Note the line #!/usr/bin/python. This line needs changed to match the location of your Python installation. For example,

    `#!/Python26/python`

    Here is an example assuming Python is installed in the C:\Python26 location

    ```
    #!/Python26/python
    print "Content-type: text/html"
    print
    print "<html><head>"
    print ""
    print "</head><body>"
    print "Hello."
    print "</body></html>"
    ```

    Save this file as test.py to your htdocs folder under your apache installation directory. Open your web browser and type in your apache host (and :port if the port is something other than 80) followed by test.py, for example

    `http://localhost/test.py`

## ref:

    * http://editrocket.com/articles/python_apache_windows.html
    * http://ian.tresman.co.uk/easyphp-cgi-script-access-denied-solved.htm
    * http://editrocket.com/articles/python_windows.html
