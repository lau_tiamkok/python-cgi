#!/Python27/python
print "Content-type: text/html; charset=utf-8"
print

# enable debugging
import cgitb
cgitb.enable()

from cassandra.cluster import Cluster
cluster = Cluster()
session = cluster.connect('demo')

result = session.execute("select * from users")
print result

print '<br/>'

for i in result:
    print i.first_name + '<br/>'
