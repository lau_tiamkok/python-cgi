# Apache Spark + Python + Cassandra

1. Install the native driver simply:

    `pip install cassandra-driver`

2. Start a python shell

    First, create an instance of a cluster, and use that to obtain a session. A session manages connections to the cluster for you. In our case, our cluster is just our local machine, but eventually you'll be connecting to a real production cluster.

    ```
    from cassandra.cluster import Cluster
    cluster = Cluster()
    session = cluster.connect('demo')
    ```

3. Pull data from `demo`,

    ```
    result = session.execute("select * from users")
    print result

    print '<br/>'

    for i in result:
        print i.first_name + '<br/>'
    ```

## Ref:

    * https://academy.datastax.com/demos/getting-started-apache-cassandra-and-python-part-i
